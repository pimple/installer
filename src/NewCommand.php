<?php

namespace Pimple\Installer\Console;

use GuzzleHttp\Client;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use ZipArchive;

class NewCommand extends Command
{
    /**
     * Configure the command options
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('new')
            ->setDescription('Create a new Pimple platform application.')
            ->addArgument('name', InputArgument::REQUIRED;
    }

    /**
     * Execute the command
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (version_compare(PHP_VERSION, '7.3.0', '<')) {
            throw new RuntimeException('The Laravel installer requires PHP 7.3.0 or greater. Please use "composer create-project laravel/laravel" instead.');
        }

        if (! class_exists('ZipArchive')) {
            throw new RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }
    
        $this->verifyApplicationDoesntExist(
            $directory = getcwd().'/'.$input->getArgument('name')
        );
        
        $output->writeln('<info>Crafting platform application...</info>');
    
        $this->download($zipFile = $this->makeFilename())
            ->extract($zipFile, $directory)
            ->clean($zipFile);
        
        $composer = $this->findComposer();
        
        $commands = [
            $composer, 'install', '--no-scripts'
        ];
    
        if ($input->getOption('no-ansi')) {
            $commands[] = '--no-ansi';
        }
        
        $process = new Process($commands, $directory, null, null, null);
    
        if ('\\' !== DIRECTORY_SEPARATOR && file_exists('/dev/tty') && is_readable('/dev/tty')) {
            $process->setTty(true);
        }
        
        $process->run(function ($type, $line) use ($output) {
            $output->write($line);
        });
        
        $output->writeln('<comment>Application ready! Start building your eCommerce application.</comment>');
        
        return 0;
    }

    /**
     * Verify if the application does not already exist
     *
     * @param string $directory
     *
     * @return void
     */
    protected function verifyApplicationDoesntExist($directory)
    {
        if (is_dir($directory)) {
            throw new RuntimeException('Application already exists!');
        }
    }

    /**
     * Generate a random temporary filename
     *
     * @return string
     */
    protected function makeFilename()
    {
        return getcwd().'/pimple_'.md5(time().uniqid()).'.zip';
    }

    /**
     * Download the temporary zip to the given file
     *
     * @param string $zipFile
     *
     * @return $this
     */
    protected function download($zipFile)
    {
        $response = (new Client)->get('https://doc-0c-a8-docs.googleusercontent.com/docs/securesc/p1maag6aog63arturnn537ooros7u0t4/da8dgak88dlr9bk3u3q04ac8efao82ok/1596824025000/00397887720408931052/00397887720408931052/1JX-Tb_VuQHzoMgI1DahKC5lNAV7eyHFX?e=download&authuser=0&nonce=9ssg3a677c0bu&user=00397887720408931052&hash=i631csj00aaku0bnhj5lkujkreidaogm');

        file_put_contents($zipFile, $response->getBody());

        return $this;
    }

    /**
     * Extract the Zip file into the given directory
     *
     * @param string $zipFile
     * @param string $directory
     *
     * @return $this
     */
    protected function extract($zipFile, $directory)
    {
        $archive = new ZipArchive;
        $archive->open($zipFile);
        $archive->extractTo($directory);
        $archive->close();
        
        return $this;
    }

    /**
     * Clean the zip file
     *
     * @param string $zipFile
     *
     * @return $this
     */
    protected function clean($zipFile)
    {
        @chmod($zipFile, 0777);
        @unlink($zipFile);

        return $this;
    }

    /**
     * Make sure the storage and bootstrap cache directories are writable
     *
     * @param string                                            $appDirectory
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return $this
     */
    protected function prepareWritableDirectories($appDirectory, OutputInterface $output)
    {
        $filesystem = new Filesystem;

        try {
            $filesystem->chmod($appDirectory.DIRECTORY_SEPARATOR.'bootstrap/cache', 0755, 0000, true);
            $filesystem->chmod($appDirectory.DIRECTORY_SEPARATOR.'storage', 0755, 0000, true);
        } catch (IOExceptionInterface $e) {
            $output->writeln('<comment>You should verify that the "storage" and "bootstrap/cache" directories are writable.</comment>');
        }

        return $this;
    }

    /**
     * Get the composer command for the environment
     *
     * @return string
     */
    protected function findComposer()
    {
        if (file_exists(getcwd().'/composer.phar')) {
            return '"'.PHP_BINARY.'" composer.phar';
        }

        return 'composer';
    }
}
