# Installation

> **Note:** This is a working in progress. Based on Laravel installer.

Pimple utilizes [Composer](https://getcomposer.org) to manage its dependencies. So, before using Pimple, make sure you have
Composer installed on your machine.

### Via Pimple Installer

First, download the Pimple installer using Composer:

```bash
composer global require pimple/installer
```

Make sure to place the ```~/.composer/vendor/bin``` directory in your ```PATH``` so the ```pimple``` executable
can be located by your system.

Once installed, the ```pimple new``` command will create a fresh Laravel installation in the directory you specify.
For instance, ```pimple new shopping-cart``` will create a directory named ```shopping-cart``` containing a fresh
Pimple installation with all of Pimple's dependencies already installed. This method of installation is much faster than
installing via Composer:

```bash
pimple new shopping-cart
```

## Via Composer Create-Project

You may also install Pimple by issuing the Composer ```create-project``` command in your terminal:

```bash
composer create-project --prefer-dist pimple/platform shopping-cart
```